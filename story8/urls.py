from django.urls import path
from .views import *

app_name = 'story8'

urlpatterns = [
    path('accordion', accordion, name='accordion'),
]