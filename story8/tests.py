from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import *
from .apps import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

# Create your tests here.
class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

    def test_story8_page_url_does_exist(self):
        response = Client().get('/accordion')
        self.assertEqual(response.status_code, 200)
    
    def test_story8_page_is_using_correct_function(self):
        found = resolve('/accordion')
        self.assertEqual(found.func, accordion)

    def test_story7_page_is_using_correct_template(self):
        response = Client().get('/accordion')
        self.assertTemplateUsed(response, 'accordion.html')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)

    def test_accordion(self):
        self.browser.get(self.live_server_url + '/accordion')

        time.sleep(5)

        self.assertIn('Profil', self.browser.page_source)
        self.assertIn('Aktivitas', self.browser.page_source)
        self.assertIn('Pengalaman', self.browser.page_source)
        self.assertIn('Prestasi', self.browser.page_source)
        self.assertIn('Syabib', self.browser.page_source)

        expbutton = self.browser.find_element_by_id('accordtest')
        expbutton.click()

        time.sleep(5)

        self.assertIn('PO SIWAK 2020', self.browser.page_source)

        upbutton = self.browser.find_element_by_id('moveuptest')
        upbutton.click()

        downbutton = self.browser.find_element_by_id('movedowntest')
        downbutton.click()

        time.sleep(5)

        page_source = self.browser.page_source
        self.assertTrue(page_source.find('Aktivitas') < page_source.find('Profil'))
        self.assertTrue(page_source.find('Pengalaman') > page_source.find('Prestasi'))

    def tearDown(self):
        self.browser.quit()
        super().tearDown()