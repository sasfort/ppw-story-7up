$(document).ready(function() {
    $(".accordiontitle").click(function() {
        if ($(event.target).is('button')) {
            event.preventDefault();
            return;
        }
        if ($(this).hasClass("open")) {
            $(this).siblings(".accordioncontent").slideUp();
            $(this).removeClass("open");
        } else {
            $(".accordiontitle").removeClass("open");
            $(this).addClass("open");
            $(".accordioncontent").slideUp();
            $(this).siblings(".accordioncontent").slideDown();
        }
    });
    $('.movedown').on('click', event => {
        const $accord = $(event.currentTarget).parents('.accord');
        $($accord).before($($accord).next());
    });
    $('.moveup').on('click', event => {
        const $accord = $(event.currentTarget).parents('.accord');
        $($accord).after($($accord).prev());
    });
  });
  