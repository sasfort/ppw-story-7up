from django.forms import ModelForm
from .models import S7

class StatusForm(ModelForm):
    class Meta:
        model = S7
        fields = ['name', 'status']