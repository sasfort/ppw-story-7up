from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import *
from .apps import *
from .models import *
from .forms import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

# Create your tests here.

class UnitTest(TestCase):
    def test_story7_page_url_does_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_page_is_using_correct_function(self):
        found = resolve('/')
        self.assertEqual(found.func, statuses)

    def test_story7_page_is_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story7.html')

    def test_confirmation_page_url_does_exist(self):
        response = Client().post('/confirmation', {'name': "syabib", 'status': "stress ppw"})
        self.assertEqual(response.status_code, 200)

    def test_confirmation_page_is_using_correct_function(self):
        found = resolve('/confirmation')
        self.assertEqual(found.func, confirmation)

    def test_confirmation_page_is_using_correct_template(self):
        response = Client().post('/confirmation', {'name': "syabib", 'status': "stress ppw"})
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_app(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')

    def test_form_validity(self):
        testform = StatusForm(data={'name': "", 'status': ""})
        self.assertFalse(testform.is_valid())
        self.assertEqual(testform.errors['name'], ["This field is required."])
        self.assertEqual(testform.errors['status'], ["This field is required."])

    def test_create_new_status(self):
        newstatus = S7(name="syabib", status="stress ppw")
        newstatus.save()
        self.assertEqual(S7.objects.all().count(), 1)

    def test_post_success(self):
        response = Client().post('/', {'name': "syabib", 'status': "stress ppw"})
        self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)

    def test_input(self):
        self.browser.get(self.live_server_url + '/')

        time.sleep(5)

        nameinput = self.browser.find_element_by_name('name')
        statusinput = self.browser.find_element_by_name('status')

        nameinput.send_keys('syabib')
        statusinput.send_keys('stress ppw')

        time.sleep(5)

        submitbutton = self.browser.find_element_by_id('submit')
        submitbutton.click()

        time.sleep(5)

        confirmbutton = self.browser.find_element_by_id('confirm')
        confirmbutton.click()

        time.sleep(5)

        self.assertIn('syabib', self.browser.page_source)
        self.assertIn('stress ppw', self.browser.page_source)

    def test_input_cancel(self):
        self.browser.get(self.live_server_url + '/')

        time.sleep(5)

        nameinput = self.browser.find_element_by_name('name')
        statusinput = self.browser.find_element_by_name('status')

        nameinput.send_keys('syabib')
        statusinput.send_keys('stress ppw')

        time.sleep(5)

        submitbutton = self.browser.find_element_by_id('submit')
        submitbutton.click()

        time.sleep(5)

        cancelbutton = self.browser.find_element_by_id('cancel')
        cancelbutton.click()

        time.sleep(5)

        self.assertNotIn('syabib', self.browser.page_source)
        self.assertNotIn('stress ppw', self.browser.page_source)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()