from django.shortcuts import render
from .models import S7
from .forms import StatusForm

# Create your views here.
def statuses(request):
    if request.method == 'POST':
        name = request.POST['name']
        status = request.POST['status']
        S7.objects.create(name = name, status = status)
    return render(request, 'story7.html', 
        {'statlist': S7.objects.all(), 'form': StatusForm()})

def confirmation(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        return render(request, 'confirmation.html',
        {'name': form.data['name'], 'status': form.data['status']})